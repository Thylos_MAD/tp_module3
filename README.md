# TP_Module3

gene : olfactory receptor 82a Spodoptera frugiperda  
accession number of this gene : LOC118277769  
accession number of the protein encoded by this gene : XP_035452568

download dataset en haut à droite.


Genome : Spodoptera frugiperda  
RefSeq:GCF_023101765.2

Bioproject : PRJNA849517. 
Spodoptera frugiperda isolate:SF20-4 RefSeq BioProject (fall armyworm)

PubMed :  
- PMID: 22301074 -> Manual GO annotation of predictive protein signatures: the InterPro approach to GO curation.  
- PMID: 30032202 -> TreeGrafter: phylogenetic tree-based annotation of proteins with Gene Ontology terms and other annotations  




LOC111354325 – odorant receptor 13a-like  
Genomique context -> location : chromosome: 18  

différents résultats blast si telecharge puis blast ou blast direct avec accession XP_

nucleotide -> contig : CADHRV010000001
Spodoptera littoralis genome assembly, contig: Chr1, whole genome shotgun sequence

Avec blast classique on peut filter le organismes d'interet

Spodoptera frugiperda
Taxonomy ID: 7108
Spodoptera littoralis
Taxonomy ID: 7109 

Publication : Identification and phylogenetics of Spodoptera frugiperda chemosensory proteins based on antennal transcriptome data
DOI: 10.1016/j.cbd.2020.100680




## EBI database

Reviewed (Swiss-Prot) (570,420)
Unreviewed (TrEMBL) (251,131,639)

18
2
Status : Unreviewed (TrEMBL) (18)



OR1
pas bcp de go therme associé
bien annoté
receptor, transductor, olfactif

BlastP swissprot
33 resultats
12 homologues
aucun lepidoptera (pas vérifié ?)

En bas de la page
Sinon Alpha fold




module load blast/2.14.0
blastp -h

-query fichier de seq
-subject banque_de_ref
-out fichier_de_sortie

Banque de donnée
/shared/bank/uniprot_swissprot/current/blast/uniprot_swissprot.pal


Ligne de commande :
blastp -query data_blast/prot.faa -db /shared/bank/uniprot_swissprot/current/blast/uniprot_swissprot -out data_blast/blastp_prot_sw.res



The public banks are here : /shared/bank/
The list of softwares are here : module avail
The command to load the last version of blastx software is : module load blast/2.14.0
The command to see the manual of blastx is : blastx -help

Run this on the IFB cluster using the sbatch command.
Add this script to a gitlab project.


script :  

\#!/bin/bash. 
blastp -query data_blast/prot.faa -db /shared/bank/uniprot_swissprot/current/blast/uniprot_swissprot -out data_blast/blastp_prot_sw.res  
  
sbatch blast_script  






## Download data

sur internet -> dataset -> ligne de command  
-> curl -> chemin du génome sur le ncbi -> donne à la commande wget  

pour les dataset il faut load l'outil  (NCBI data set cli)

==>> module load ncbi-datasets-cli/14.19.0
maintenant on peut utiliser la commande dataset

exemple : 

[app-2401-11@cpu-node-24 data_blast]$ module load ncbi-datasets-cli/14.19.0
[app-2401-11@cpu-node-24 data_blast]$ mkdir genome 
[app-2401-11@cpu-node-24 data_blast]$ cd genome 
[app-2401-11@cpu-node-24 genome]$ datasets download genome accession GCF_023101765.2 --include gff3,rna,cds,protein,genome,seq-report
Warning: Current version of client does not support full genome reports! New version of client (16.3.0) available at https://ftp.ncbi.nlm.nih.gov/pub/datasets/command-line/LATEST/linux-amd64/datasets
Collecting 1  records [================================================] 100% 1/1
Downloading: ncbi_dataset.zip    4.95MB 772kB/s





Avec wget (attention 0 majuscule):  
wget -O genome_wget_verion "https://api.ncbi.nlm.nih.gov/datasets/v2alpha/genome/accession/GCF_023101765.2/download?include_annotation_type=GENOME_FASTA"


## SRA  
NCI -> SRA ->  Spodoptera frugiperda infected by JcDV   
https://trace.ncbi.nlm.nih.gov/Traces/?view=run_browser&acc=SRR20387192&display=metadata  
run identifiant -> SRR20387192   

module load sra-tools/2.11.0  
fastq-dump -help  
fastq-dump SRR20387192    


format : name, seq, sens, score  
probleme les read forward et reverses on étés mis dans le même fichier  
-> mettre l'option split files  
fastq-dump --split-files SRR20387192.fastq




